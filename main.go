package main

import (
	"io/ioutil"
	"os"
	"path"
	"strings"

	log "github.com/Sirupsen/logrus"
)

func checkError(err error) {
	if err != nil {
		log.Errorf("Fatal error: %s", err.Error())
		os.Exit(0)
	}
}

func MustGetenv(name string) string {
	s := os.Getenv(name)
	if s == "" {
		log.Errorf("%s is required.", name)
		os.Exit(0)
	}
	return s
}

func main() {
	debug := strings.ToLower(os.Getenv("DEBUG"))
	if debug == "true" || debug == "yes" || debug == "1" {
		log.SetLevel(log.DebugLevel)
	}

	rancherUrl := MustGetenv("RANCHER_URL")
	accessKey := MustGetenv("RANCHER_ACCESS_KEY")
	secretKey := MustGetenv("RANCHER_SECRET_KEY")
	serviceName := MustGetenv("SERVICE_NAME")
	parts := strings.Split(serviceName, "/")
	if len(parts) != 2 {
		log.Errorf("Format of SERVICE_NAME is STACK/SERVICE. Found %s", serviceName)
	}

	stackName, serviceName := parts[0], parts[1]

	c := Connect(rancherUrl, accessKey, secretKey)
	stackId := c.getStackId(stackName)
	service := c.getService(serviceName, stackId)
	instances := c.getInstancesFromService(service)

	conf := NewConfigHandler(rancherUrl, accessKey, secretKey, c)
	conf.serviceId = service.Id
	conf.outputFile = os.Getenv("OUTPUT_FILE")
	if conf.outputFile == "" {
		conf.outputFile = "/etc/rancher-gen/config"
	}

	dir := path.Dir(conf.outputFile)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		log.Errorf("Could not create output directory: %s", err.Error())
		return
	}

	templateFile := os.Getenv("TEMPLATE_FILE")
	if templateFile != "" {
		if b, err := ioutil.ReadFile(templateFile); err == nil {
			conf.templateString = string(b)
		}
	} else {
		conf.templateString = os.Getenv("TEMPLATE")
	}

	conf.notify = os.Getenv("NOTIFY_CMD")

	for _, instance := range instances {
		log.Infof("Add instance %s (%s)", instance.Name, instance.Id)
		conf.Config.Instances = append(conf.Config.Instances, Instance{
			Id:               instance.Id,
			Hostname:         instance.Hostname,
			Name:             instance.Name,
			PrimaryIpAddress: instance.PrimaryIpAddress,
			ServiceIds:       []string{service.Id},
			State:            instance.State,
			Transitioning:    instance.Transitioning,
		})
	}

	// write base config
	conf.writeConfig()

	if err := conf.subscribe(); err != nil {
		log.Errorf("Failed to subscribe: %s", err.Error())
		return
	}
}
