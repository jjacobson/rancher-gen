# rancher-gen

A service for Rancher that uses the Rancher client and websocket to listen for changes in a service.

## Usage
This is an example `docker-compose`
```yaml
version: '2'
services:
  rancher-gen:
    image: 'registry.gitlab.com/jjacobson/rancher-gen:1.0.0'
    environment:
      RANCHER_URL: 'http://rancher/v2-beta/projects/1a293'
      RANCHER_ACCESS_KEY: 'abcdefg'
      RANCHER_SECRET_KEY: '1234567890'
      SERVICE_NAME: store/etcd
```

### Environment Variables
Name                | Required | Description
--------------------|----------|---------------------
RANCHER_URL         | Yes      | The URL to the Rancher API
RANCHER_ACCESS_KEY  | Yes      | The access key for the Rancher API
RANCHER_SECRET_KEY  | Yes      | The secret key for the Rancher API
SERVICE_NAME        | Yes      | The service name to watch of the format `STACK`/`SERVICE`
OUTPUT_FILE         | No       | The file to write the config to. If the directory does not exist, it will be created. Defaults to `/etc/rancher-gen/config`
TEMPLATE_FILE       | No       | The file that contains the template. This gets checked before `TEMPLATE`.
TEMPLATE            | No       | The template string to be used if `TEMPLATE_FILE` doesn't exist. If neither exists, the default template is used as defined in `config.go`
NOTIFY_CMD          | No       | The command to be run by the shell when the config changes.
DEBUG               | No       | Whether or not to turn on debug logging. Defaults to `false`. Acceptable values are `true`, `yes`, or `1`

## License
MIT
