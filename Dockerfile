FROM golang:1.8-alpine3.6

WORKDIR /go/src/app
COPY *.go ./

RUN apk --update add git && \
    go-wrapper download && \
    go-wrapper install && \
    apk del git

CMD ["go-wrapper", "run"]
