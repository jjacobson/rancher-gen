package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/websocket"
)

var slashRegex = regexp.MustCompile("[/]{2,}")

func subscribeToEvents(subscribeURL string, accessKey string, secretKey string, data url.Values) (*websocket.Conn, error) {
	// gorilla websocket will blow up if the path starts with //
	parsed, err := url.Parse(subscribeURL)
	if err != nil {
		return nil, err
	}

	var scheme string
	if parsed.Scheme == "https" || parsed.Scheme == "wss" {
		scheme = "wss"
	} else {
		scheme = parsed.Scheme
	}

	dialer := &websocket.Dialer{
		HandshakeTimeout: time.Second * 30,
	}
	headers := http.Header{}
	headers.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(accessKey+":"+secretKey)))
	subscribeURL = fmt.Sprintf("%s://%s%s/subscribe?%s", scheme, parsed.Host, parsed.Path, data.Encode())
	ws, resp, err := dialer.Dial(subscribeURL, headers)

	if err != nil {
		log.WithFields(log.Fields{
			"subscribeUrl": subscribeURL,
		}).Errorf("Error subscribing to events: %s", err)
		if resp != nil {
			log.WithFields(log.Fields{
				"status":          resp.Status,
				"statusCode":      resp.StatusCode,
				"responseHeaders": resp.Header,
			}).Error("Got error response")
			if resp.Body != nil {
				defer resp.Body.Close()
				body, _ := ioutil.ReadAll(resp.Body)
				log.Errorf("Error response: %s", body)
			}
		}
		if ws != nil {
			ws.Close()
		}
		return nil, err
	}
	return ws, nil
}
