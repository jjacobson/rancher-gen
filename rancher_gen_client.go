package main

import (
	"os"

	log "github.com/Sirupsen/logrus"
	rancher "github.com/rancher/go-rancher/v2"
)

type RancherGenClient struct {
	*rancher.RancherClient
}

func Connect(url, accessKey, secretKey string) *RancherGenClient {
	opts := rancher.ClientOpts{
		Url:       url,
		AccessKey: accessKey,
		SecretKey: secretKey,
	}

	client, err := rancher.NewRancherClient(&opts)
	checkError(err)
	return &RancherGenClient{client}
}

func (c *RancherGenClient) getStackId(name string) string {
	filters := map[string]interface{}{"name": name}
	opts := rancher.ListOpts{Filters: filters}
	result, err := c.Stack.List(&opts)
	checkError(err)
	stacks := result.Data
	if len(stacks) != 1 {
		log.Errorf("Could not find stack %s.", name)
		os.Exit(0)
	}
	return stacks[0].Id
}

func (c *RancherGenClient) getService(name string, stackId string) rancher.Service {
	filters := map[string]interface{}{
		"name":    name,
		"stackId": stackId,
	}
	opts := rancher.ListOpts{Filters: filters}
	result, err := c.Service.List(&opts)
	checkError(err)
	services := result.Data
	if len(services) != 1 {
		log.Errorf("Could not find stack %s.", name)
		os.Exit(0)
	}
	return services[0]
}

func (c *RancherGenClient) getInstancesFromService(service rancher.Service) []rancher.Container {
	containers := &rancher.ContainerCollection{}
	checkError(c.GetLink(service.Resource, "instances", containers))
	return containers.Data
}
